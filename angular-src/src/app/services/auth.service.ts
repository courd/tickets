import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import { tokenNotExpired } from 'angular2-jwt'
import { map } from 'rxjs/operators';


let forHeroku = true;
let baseURL = "//localhost:8080/"

if (forHeroku) baseURL = ''

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  authToken: any;
  user: any;

  constructor(private http:Http) { }


  registerUser(user){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(baseURL + 'users/register', user, {headers: headers})
      .pipe(map(res => res.json()));
  }

  authenticateUser(user){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(baseURL + 'users/authenticate', user, {headers: headers})
      .pipe(map(res => res.json()));
  }

  getProfile(){
    let headers = new Headers();
    this.loadToken();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authToken);
    return this.http.get(baseURL + 'users/profile', {headers: headers})
      .pipe(map(res => res.json()));
  }

  postTicket(ticket, myCallback){
    fetch(baseURL + 'tickets/register', {
    method: 'post',
    body: JSON.stringify(ticket),
  headers: {
    "Content-Type": "application/json"
  }
  }).then(function(response) {
    return response.json();
  }).then(function(data) {
    console.log(JSON.stringify(data, undefined, 2));
    myCallback()
  });
  }

  addNote(notes, id){
    fetch(baseURL + 'tickets/' + id + "/notes", {
    method: 'post',
    body: JSON.stringify({notes}),
  headers: {
    "Content-Type": "application/json"
  }
  }).then(function(response) {
    return response.json();
  }).then(function(data) {
    console.log(JSON.stringify(data, undefined, 2));
  });
  }

  updateTicket(ticket, id, myCallback){
    console.log(ticket)
    fetch(baseURL + 'tickets/update/' + id, {
    method: 'post',
    body: JSON.stringify(ticket),
  headers: {
    "Content-Type": "application/json"
  }
  }).then(function(response) {
    return response.json();
  }).then(function(data) {
    console.log(JSON.stringify(data, undefined, 2));
    myCallback()
  });
  }


  getTickets(){
    let headers = new Headers();
    this.loadToken();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authToken);
    return this.http.get(baseURL + 'tickets', {headers: headers})
      .pipe(map(res => res.json()));
  }

  getTicket(id){

    let headers = new Headers();
    this.loadToken();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authToken);
    return this.http.get(baseURL + 'tickets/' + id, {headers: headers})
      .pipe(map(res => res.json()));

  }

  deleteTicket(id){

    fetch(baseURL + 'tickets/' + id, {
    method: 'post',
    body: '{"delete": "delete"}',
  headers: {
    "Content-Type": "application/json"
  }
  }).then(function(response) {
    return response.json();
  }).then(function(data) {
    console.log(JSON.stringify(data, undefined, 2));
  });

  }

  storeUserData(token, user){
    localStorage.setItem("id_token", token);
    localStorage.setItem("user", JSON.stringify(user));
    this.authToken = token;
    this.user = user;
  }

  loadToken() {
    const token = localStorage.getItem('id_token');
    this.authToken = token;
  }

  loggedIn(){
    return tokenNotExpired('id_token');
  }

  logout(){
    this.authToken = null;
    this.user = null;
    localStorage.clear();
  }

}
