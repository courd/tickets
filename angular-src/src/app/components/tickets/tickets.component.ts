import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {Router} from '@angular/router'
import {FlashMessagesService} from 'angular2-flash-messages';


@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.css']
})
export class TicketsComponent implements OnInit {
  tickets: any;
  allTickets: any;
  ticketsExist: Boolean;
  department: String;
  status: String;

  constructor(
    private flashMessage:FlashMessagesService,
    private AuthService:AuthService,
    private Router:Router
) { }


  ngOnInit() {
    this.AuthService.getTickets().subscribe(tickets => {
      this.tickets = tickets.tickets;
      this.allTickets = tickets.tickets;
        if (this.tickets[0]) {this.ticketsExist = true}
    },
  err => {
    console.log(err);
    return false;
  })

  }

  deleteTicket(id) {
    this.AuthService.deleteTicket(id);
    this.flashMessage.show("Ticket Deleted", {
      cssClass: 'alert-success',
       timeout: 3000
     })
this.tickets = this.tickets.filter((e) => e._id !== id)

  }

  addNote(id) {
    const note = prompt("your note")
    if (note && note.length > 1) {
    this.tickets.find((e) => e._id == id).notes.push(note)
    // console.log(this.tickets[0].notes)
    this.flashMessage.show("Note Added", {
      cssClass: 'alert-success',
       timeout: 3000
     })
     this.AuthService.addNote(this.tickets[0].notes, id);
   }
  }

  applyFilters() {
    this.tickets = this.allTickets
    if (this.status !== "All" && this.status !== undefined){
    this.tickets = this.allTickets.filter((e) => e.status === this.status)
  }
  if (this.department !== "All" && this.department !== undefined){
  this.tickets = this.tickets.filter((e) => e.department === this.department)
  
}

  }



}
