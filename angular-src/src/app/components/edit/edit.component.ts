import { Component, OnInit } from '@angular/core';
import {FlashMessagesService} from 'angular2-flash-messages';
import {Router} from '@angular/router';
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  title: String;
  description: String;
  department: String;
  status: String;
  id: String;
  ticketData: Object;

  constructor(
    private flashMessage:FlashMessagesService,
    private AuthService:AuthService,
    private Router:Router
  ) { }

  ngOnInit() {
    this.id = window.location.href.substring(window.location.href.length - 24, window.location.href.length)
    this.AuthService.getTicket(this.id).subscribe(ticket => {
      this.ticketData = ticket.ticket;
      this.title = ticket.ticket.title;
      this.description = ticket.ticket.description;
      this.department = ticket.ticket.department;
      this.status = ticket.ticket.status;
    },
  err => {
    console.log(err);
    return false;
  })

  }

  onRegisterSubmit(){

    const myCallback = () => {
      this.Router.navigate(['/dashboard']);
    }
    const ticket = {
      title: this.title,
      description: this.description,
      department: this.department,
      status: this.status
    }
    this.AuthService.updateTicket(ticket, this.id, myCallback)
      this.flashMessage.show("Ticket Updated", {cssClass: 'alert-success', timeout: 3000});

  }

}
