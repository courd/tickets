import { Component, OnInit } from '@angular/core';
import {FlashMessagesService} from 'angular2-flash-messages';
import {Router} from '@angular/router';
import {AuthService} from "../../services/auth.service";


@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  title: String;
  description: String;
  department: String;
  user:any;

  constructor(
    private flashMessage:FlashMessagesService,
    private AuthService:AuthService,
    private Router:Router
  ) { }

  ngOnInit() {
    this.AuthService.getProfile().subscribe(profile => {
      this.user = profile.user;
    },
    err => {
      console.log(err);
      return false;
    })
  }


    onRegisterSubmit(){
      const ticket = {
        title: this.title,
        description: this.description,
        department: this.department,
        createdBy: this.user.name
      }
      const myCallback = () => {
        this.Router.navigate(['/dashboard']);
      }
      this.AuthService.postTicket(ticket, myCallback)
        this.flashMessage.show("Ticket Added", {cssClass: 'alert-success', timeout: 3000});

    }

}
