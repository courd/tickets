import { Component, OnInit } from '@angular/core';
import {FlashMessagesService} from 'angular2-flash-messages';
import {AuthService} from "../../services/auth.service";
import {Router} from '@angular/router'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username: String;
  password: String;

  constructor(
      private flashMessage:FlashMessagesService,
      private AuthService:AuthService,
      private Router:Router
    ) { }

  ngOnInit() {
  }

  onLoginSubmit(){
    const user = {
      username: this.username,
      password: this.password
    }
    this.AuthService.authenticateUser(user).subscribe(data => {
      if(data.success){
        this.AuthService.storeUserData(data.token, data.user)
        this.flashMessage.show("You are logged in", {
          cssClass: 'alert-success',
           timeout: 5000
         })
         this.Router.navigate(['dashboard'])
      } else {
        this.flashMessage.show(data.msg, {
          cssClass: 'alert-danger',
           timeout: 5000
         })
         this.Router.navigate(['login'])
      }
    })
  }

}
