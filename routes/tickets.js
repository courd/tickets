const express = require('express');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const Ticket = require('../models/ticket');
const {ObjectID} = require('mongodb');
const _ = require('lodash')
const moment = require("moment");

const router = express.Router();

// create

router.post('/register', (req, res, next) => {
  let newTicket = new Ticket({
    title: req.body.title,
    description: req.body.description,
    department: req.body.department,
    createdBy: req.body.createdBy,
    dateCreated: new moment().utcOffset(-7).format("ddd, MM D YYYY, h:mm a"),
    lastModified: new moment().utcOffset(-7).format("ddd, MM D YYYY, h:mm a")
  });

  Ticket.addTicket(newTicket, (err, ticket) => {
    if(err){
      res.json({success: false, msg:'Failed to add ticket'});
    } else {
      res.json({success: true, msg:'Ticket added' + ticket});
    }
  });

  });

  //read

  router.get('/:id', (req, res, next) => {
    const id = req.params.id
    Ticket.getTicketById(id, (err, ticket) => {
      res.json({ticket});
    })
  });

  router.get('/', (req, res, next) => {
    Ticket.getTickets((err, tickets) => {
      res.json({tickets});
    })
  });


  //update

  router.post('/update/:id', (req, res) => {
  const id = req.params.id
  const body = _.pick(req.body, ['title', 'status', "description", 'department'])

  if (!ObjectID.isValid(id)){
    res.status(404).send("ID not Valid");
  }

  if (body.status === "Closed") {
    body.completedAt = new moment().utcOffset(-7).format("ddd, MM D YYYY, h:mm a")
  } else {
    body.completedAt = null
  }

  body.lastModified = new moment().utcOffset(-7).format("ddd, MM D YYYY, h:mm a")

  Ticket.findByIdAndUpdate(id, {$set: body}, {new: true}).then((ticket) => {
    ticket ? res.status(200).send({ticket}) : res.status(404).send("Id not found")
  }).catch((e) => {
    res.status(400).send()
  })
})

// notes

router.post('/:id/notes', (req, res) => {
const id = req.params.id
const body = _.pick(req.body, ['notes'])

if (!ObjectID.isValid(id)){
  res.status(404).send("ID not Valid");
}

body.lastModified = new moment().utcOffset(-7).format("ddd, MM D YYYY, h:mm a")

Ticket.findByIdAndUpdate(id, {$set: body}, {new: true}).then((ticket) => {
  ticket ? res.status(200).send({ticket}) : res.status(404).send("Id not found")
}).catch((e) => {
  res.status(400).send()
})
})

  //delete

  router.post("/:id", (req, res, next) => {
  const id = req.params.id
  if (!ObjectID.isValid(id)){
    res.status(404).send("ID not Valid");
  }
  Ticket.findByIdAndRemove(id).then((ticket) => {
    ticket ? res.status(200).send({success: true, ticket}) : res.status(404).send("Id not found")
  }).catch((e) => res.status(400).send(e))
})


module.exports = router;
