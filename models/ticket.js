const mongoose = require('mongoose')
const config = require('../config/database');
const moment = require("moment");

const TicketSchema = mongoose.Schema({
  title: {
    type: String,
    default: "No Title",
    minlength: 1,
    trim: true
  },
  createdBy: {
    type: String,
    default: "John Doe",
    trim: true
  },
    description: {
    type: String,
    default: "No Description",
    minlength: 1,
    trim: true
  },
  status: {
    type: String,
    default: "Open"
  },
  completedAt: {
    type: String,
    default: null
  },
  dateCreated: {
    type: String,
    default: new moment().utcOffset(-7).format("ddd, MM D YYYY, h:mm a")
  },
  lastModified: {
    type: String,
    default: new moment().utcOffset(-7).format("ddd, MM D YYYY, h:mm a")
  },
  department: {
    type: String,
    default: "Sales"
  },
  notes: {
    type: Array
  }
})

const Ticket = module.exports = mongoose.model('Ticket', TicketSchema);

module.exports.addTicket = function(newTicket, callback){
      newTicket.save(callback);
}

module.exports.getTicketById = function(id, callback){
  Ticket.findById(id, callback);
}

module.exports.getTickets = function(callback){
  Ticket.find(callback);
}
